class CreateNuevos < ActiveRecord::Migration[5.0]
  def change
    create_table :nuevos do |t|
      t.integer :fuerza

      t.timestamps
    end
  end
end
